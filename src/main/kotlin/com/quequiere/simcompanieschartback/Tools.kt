package com.quequiere.simcompanieschartback

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Tools {
    companion object{
        inline fun <reified T> logger(): Logger {
            return LoggerFactory.getLogger(T::class.java)
        }
    }
}