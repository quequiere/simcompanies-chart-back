package com.quequiere.simcompanieschartback.batch

import com.quequiere.simcompanieschartback.Tools.Companion.logger
import com.quequiere.simcompanieschartback.persistence.repository.PriceRowRepository
import com.quequiere.simcompanieschartback.provider.ScProvider
import com.quequiere.simcompanieschartback.service.ItemService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class PriceBatch {

    @Autowired
    lateinit var scProvider: ScProvider

    @Autowired
    lateinit var priceRowRepository: PriceRowRepository

    @Autowired
    lateinit var itemService: ItemService

    @Scheduled(cron = "0 */2 * * * ?")
    fun refreshPrice() {
        val prices = scProvider.getLastPrice()
        val priceRow = itemService.marketTickerToPriceRow(prices)
        priceRowRepository.save(priceRow)
        logger<PriceBatch>().info("Refreshed price")
    }
}