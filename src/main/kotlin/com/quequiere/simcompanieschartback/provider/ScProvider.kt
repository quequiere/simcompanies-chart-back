package com.quequiere.simcompanieschartback.provider

import com.quequiere.simcompanieschartback.provider.models.MarketTickerResponse
import com.quequiere.simcompanieschartback.provider.models.ResourceDetail
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class ScProvider {

    fun getLastPrice(): List<MarketTickerResponse> {
        val rest = RestTemplate()
        val response = rest.getForEntity("https://www.simcompanies.com/api/v1/market-ticker/2022-03-27T11:16:33.952Z/", Array<MarketTickerResponse>::class.java)
        return response.body!!.toList()
    }

    fun getItemsInfo(): List<ResourceDetail> {
        val rest = RestTemplate()
        val response = rest.getForEntity("https://www.simcompanies.com/api/v3/en/encyclopedia/resources/", Array<ResourceDetail>::class.java)
        return response.body!!.toList()
    }
}