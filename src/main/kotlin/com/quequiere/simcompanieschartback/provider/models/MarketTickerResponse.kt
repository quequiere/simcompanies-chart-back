package com.quequiere.simcompanieschartback.provider.models

data class MarketTickerResponse(val kind: Int, val image: String, val price: Double, val is_up: Boolean)
