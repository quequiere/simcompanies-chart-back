package com.quequiere.simcompanieschartback.provider.models

data class ResourceDetail(
    val name: String,
    val image: String,
    val db_letter: Int,
    val transportation: Double,
    val retailable: Boolean,
    val research: Boolean
)