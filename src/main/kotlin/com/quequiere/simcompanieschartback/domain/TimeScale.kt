package com.quequiere.simcompanieschartback.domain

enum class TimeScale {
    MINUTE_5,
    MINUTE_15,
    HOUR_1,
    HOUR_4,
    DAY,
}