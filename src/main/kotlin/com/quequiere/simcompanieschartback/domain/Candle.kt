package com.quequiere.simcompanieschartback.domain

import org.joda.time.DateTime

data class Candle(
    val date: DateTime,
    var o: Double = 0.0,
    var c: Double = 0.0,
    var h: Double = 0.0,
    var l: Double = 0.0,
)