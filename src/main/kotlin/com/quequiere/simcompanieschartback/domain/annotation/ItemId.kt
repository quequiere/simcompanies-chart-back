package com.quequiere.simcompanieschartback.domain.annotation

//@Target( AnnotationTarget.VALUE_PARAMETER)
//@Retention(AnnotationRetention.BINARY, AnnotationRetention.RUNTIME)
annotation class ItemId(val id : Int)
