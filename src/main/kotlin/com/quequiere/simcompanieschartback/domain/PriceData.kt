package com.quequiere.simcompanieschartback.domain

import org.joda.time.DateTime

data class PriceData(
    val time : DateTime,
    val price : Double
)