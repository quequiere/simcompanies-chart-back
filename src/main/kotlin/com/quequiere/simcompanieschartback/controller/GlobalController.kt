package com.quequiere.simcompanieschartback.controller

import com.quequiere.simcompanieschartback.domain.Candle
import com.quequiere.simcompanieschartback.domain.TimeScale
import com.quequiere.simcompanieschartback.provider.models.ResourceDetail
import com.quequiere.simcompanieschartback.service.ItemService
import com.quequiere.simcompanieschartback.service.PriceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
@RestController
class GlobalController {
    @Autowired
    lateinit var priceService: PriceService

    @Autowired
    lateinit var itemService: ItemService

    @GetMapping("/api/v1/price/{timescale}/{itemID}")
    fun getPrice(@PathVariable timescale: TimeScale, @PathVariable itemID: Int): List<Candle> {
        return priceService.getCandlesByTimescale(timescale, itemID)
    }

    @GetMapping("/api/v1/resources")
    fun getItems(): List<ResourceDetail> {
        return itemService.getItems()
    }
}