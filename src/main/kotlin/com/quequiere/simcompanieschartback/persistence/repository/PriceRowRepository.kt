package com.quequiere.simcompanieschartback.persistence.repository

import com.quequiere.simcompanieschartback.persistence.models.PriceRow
import org.joda.time.DateTime
import org.springframework.data.mongodb.repository.MongoRepository

interface PriceRowRepository : MongoRepository<PriceRow, DateTime> {
}