package com.quequiere.simcompanieschartback.service

import com.quequiere.simcompanieschartback.domain.PriceData
import com.quequiere.simcompanieschartback.domain.annotation.ItemId
import com.quequiere.simcompanieschartback.persistence.models.PriceRow
import com.quequiere.simcompanieschartback.provider.ScProvider
import com.quequiere.simcompanieschartback.provider.models.MarketTickerResponse
import com.quequiere.simcompanieschartback.provider.models.ResourceDetail
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.reflect.Field

@Service
class ItemService {

    @Autowired
    lateinit var scProvider: ScProvider

    private var cachedItems: List<ResourceDetail> = listOf()

    fun priceRowToPriceData(priceRow: PriceRow, itemID: Int): PriceData {
        val result = getField(itemID).get(priceRow) as Double
        return PriceData(priceRow.time, result)
    }

    fun marketTickerToPriceRow(marketTickerResponse: List<MarketTickerResponse>): PriceRow {
        val priceRow = PriceRow()
        priceRow.time = DateTime.now()
        marketTickerResponse.forEach {
            val field = getField(it.kind)
            field.set(priceRow, it.price)
        }
        return priceRow
    }

    private fun getField(itemID: Int): Field {
        try {
            val field = PriceRow::class.java.declaredFields
                .filter { it.getAnnotation(ItemId::class.java) != null }
                .map { Pair(it.getAnnotation(ItemId::class.java).id, it) }
                .filter { it.first == itemID }
                .first().second
            field.isAccessible = true
            return field
        } catch (e: NoSuchElementException) {
            println("Failed to find field for item $itemID")
            throw e
        }

    }

    fun getFieldName(itemID: Int): String {
        return getField(itemID).name
    }

    fun getItems(): List<ResourceDetail> {
        if (cachedItems.size <= 0) {
            cachedItems = scProvider.getItemsInfo().sortedBy { it.name }
            cachedItems = cachedItems.filter {
                try{
                    getFieldName(it.db_letter)
                    true
                }catch (e : Exception){
                    false
                }
            }
        }



        return cachedItems
    }
}