package com.quequiere.simcompanieschartback.service

import com.quequiere.simcompanieschartback.domain.Candle
import com.quequiere.simcompanieschartback.domain.TimeScale
import com.quequiere.simcompanieschartback.persistence.models.PriceRow
import com.quequiere.simcompanieschartback.persistence.repository.PriceRowRepository
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.findOne
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Service
import kotlin.math.truncate

@Service
class PriceService {

    @Autowired
    lateinit var itemService: ItemService

    @Autowired
    lateinit var priceCalculator: PriceCalculator

    @Autowired
    lateinit var priceRowRepository: PriceRowRepository

    @Autowired
    lateinit var mongoTemplate: MongoTemplate


    fun getCandlesByTimescale(timeScale: TimeScale, itemID: Int): List<Candle> {
        val fieldToRet = itemService.getFieldName(itemID);
        val query = Query()
        query.fields().include(fieldToRet)
        query.fields().include("time")

       val lightRequest =  mongoTemplate.find(query,PriceRow::class.java)
        val candles = lightRequest
            .sortedBy { it.time }
            .map { itemService.priceRowToPriceData(it, itemID) }
            .filter { it.price != 0.0 }
            .groupBy { datetimeToReferenceScale(it.time, timeScale) }
            .map { priceCalculator.computeCandles(it.value, it.key) }

        candles.forEachIndexed { index, candle ->
            if (index != 0) {
                candle.o = candles[index - 1].c
            }
        }
        return candles
    }

    fun datetimeToReferenceScale(dateTime: DateTime, timeScale: TimeScale): DateTime {
        val referenceTime = when (timeScale) {
            TimeScale.MINUTE_5 -> dateTime.withMillisOfSecond(0).withSecondOfMinute(0)
                .withMinuteOfHour(truncate(dateTime.minuteOfHour().get() / 5.0).toInt())

            TimeScale.MINUTE_15 -> dateTime.withMillisOfSecond(0).withSecondOfMinute(0)
                .withMinuteOfHour(truncate(dateTime.minuteOfHour().get() / 15.0).toInt() * 15)

            TimeScale.HOUR_1 -> dateTime.withMillisOfSecond(0).withSecondOfMinute(0)
                .withMinuteOfHour(0)

            TimeScale.HOUR_4 -> dateTime.withMillisOfSecond(0).withSecondOfMinute(0)
                .withMinuteOfHour(0).withHourOfDay(truncate(dateTime.hourOfDay().get() / 4.0).toInt() * 4)

            TimeScale.DAY -> dateTime.withMillisOfSecond(0).withSecondOfMinute(0)
                .withMinuteOfHour(0).withHourOfDay(0)
        }
        return referenceTime
    }
}