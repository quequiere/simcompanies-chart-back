package com.quequiere.simcompanieschartback.service

import com.quequiere.simcompanieschartback.provider.ScProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.nio.channels.Channels
import java.nio.channels.FileChannel
import java.nio.channels.ReadableByteChannel
import javax.annotation.PostConstruct


/**
 * Help to download image for frontend
 */
//@Service
class ImageService {

    @Autowired
    lateinit var scProvider: ScProvider

    @PostConstruct
    fun afterStart() {
        val links = scProvider.getLastPrice().map { it.image }
        downloadImage(links)
    }

    fun downloadImage(partUri: List<String>) {
        val directory = File("img_output")
        if (!directory.exists()) {
            directory.mkdir()
        }

        val baseURI = "https://d1fxy698ilbz6u.cloudfront.net/static/"
        val ready = partUri.map { Pair(it, it.split("/")[2]) }
        ready.forEach { writeFile(URL(baseURI + it.first), it.second) }
    }

    fun writeFile(url: URL, filename: String) {
        val readableByteChannel: ReadableByteChannel = Channels.newChannel(url.openStream())
        val fileOutputStream = FileOutputStream("img_output/${filename}")
        val fileChannel: FileChannel = fileOutputStream.channel
        fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
    }
}