package com.quequiere.simcompanieschartback.service

import com.quequiere.simcompanieschartback.domain.Candle
import com.quequiere.simcompanieschartback.domain.PriceData
import org.joda.time.DateTime
import org.springframework.stereotype.Service

@Service
class PriceCalculator {
    fun computeCandles(inputPrices: List<PriceData>, referenceTime: DateTime): Candle {
        val candle = Candle(referenceTime)
        val reOrdered = inputPrices.sortedBy { it.time.millis }
        candle.o = reOrdered.first().price
        candle.c = reOrdered.last().price
        candle.h = reOrdered.maxOf { it.price }
        candle.l = reOrdered.minOf { it.price }
        return candle
    }
}