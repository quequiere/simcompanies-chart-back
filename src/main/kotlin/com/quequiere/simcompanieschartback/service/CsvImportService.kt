package com.quequiere.simcompanieschartback.service

import com.opencsv.CSVReader
import com.quequiere.simcompanieschartback.persistence.models.PriceRow
import com.quequiere.simcompanieschartback.persistence.repository.PriceRowRepository
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.nio.file.Files
import java.nio.file.Paths
import java.text.SimpleDateFormat
import javax.annotation.PostConstruct


@Service
class CsvImportService {

    @Autowired
    lateinit var priceRowRepository: PriceRowRepository

    @Value("classpath:csv/latest.csv")
    lateinit var resourceFile: Resource

    @PostConstruct
    fun afterInit() {
        readCsv()
    }

    fun readCsv() {
        if (priceRowRepository.count() > 0) return;
        println("Inject csv to database")

        val finalResult = mutableListOf<PriceRow>()

        val reader: Reader = BufferedReader(InputStreamReader(resourceFile.inputStream))
        val csvReader = CSVReader(reader)
        csvReader.readNext()
        var line: Array<String>?
        while (csvReader.readNext().also { line = it } != null) {
            val fields = PriceRow::class.java.declaredFields.toMutableList()
            fields.removeFirst();

            val df = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
            val parsedDate = df.parse(line?.get(0))
            val priceRow = PriceRow()
            priceRow.time = DateTime(parsedDate)

            fields.forEachIndexed { index, it ->
                it.isAccessible = true
                val currentValue =
                    line?.get(index + 1) ?: throw java.lang.RuntimeException("Failed to get value from line")

                val price = currentValue.replace(",", "")
                if (price.isNotBlank()) {
                    it.set(priceRow, price.toDouble())
                }
            }
            finalResult.add(priceRow)
        }
        reader.close()
        csvReader.close()
        priceRowRepository.saveAll(finalResult.reversed())
    }
}